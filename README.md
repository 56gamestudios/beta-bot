# Discord Beta Bot

A super simple Discord bot for assigning beta keys to users.

Requires that a user send *any* message to a specific channel in order to receive instructions.

## Issues

Sometimes the bot will crash. If that happens, the bot will appear offline in Discord. This was pretty rare in my experience, but one user seemed to make it crash whenever they used the `!key` command, so I just manually assigned them to a key.

## Setup

Configure the bot by editing `auth.json` and `config.json` and then just run it (I think the command should be `node main.js`).

### `auth.json`

Get a token from the [Discord Developer Portal](https://discordapp.com/developers/applications) under the bots section of your application's settings.

### `config.json`

1. Define your admins &mdash; can add and assign keys
1. Set your channel &mdash; the first time a user sends a message in this channel they will receive instructions for how to get a key
1. Customize your bot's messages

### `data.json`

You could manually specify the available keys in this file or add them using the `!add-keys` command.

If you need to fix an issue (e.g., unassign a key, blacklist a user by assigning them an invalid key), you can do so by stopping the bot, modifying this file, and starting the bot again afterward.

#### Example

```
{
	"dmSent": {
		// true if the bot already DM'd instructions
		"<username#discriminator>": <was sent>
	},
	"assignments": {
		"<username#discriminator>": "<key>"
	},
	"keys": {
		// true if the key hasn't been assigned
		"<key>": <is available>
	}
}
```

## Commands

Commands are sent to the bot via DM.

### `!key`

Will assign you a key if one is available and you haven't already been assigned a key.

### `!add-keys <key-1> <key-2> .. <key-N>`

To add more keys to the system. You can pass any number of keys, with spaces in between each.

### `!assign <username#discriminator> <key>`

To assign a key to a user.

Technically "`username#discriminator`" could be anything you want, but if you use a Discord username and discriminator that user won't be able to get another key from the bot. You can also specify a key that's invalid or not in the system yet, and it will add it to the system and assign it to the user at the same time.

## License

This project uses the MIT license. See LICENSE file for details.
