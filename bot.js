import { read } from "json-file";
import { keys, omitBy } from "lodash";
import { token } from "./auth.json";
import { admins, betaChannel, messages } from "./config.json";
import template from "es6-template-strings";
import * as Discord from "discord.js";
import * as log from "winston";

// Configure logger settings
log.remove(log.transports.Console);
log.add(log.transports.Console, {
	colorize: true
});
log.level = 'debug';

// Load persisted data
const data = read("./data.json");

// Initialize bot
// const bot = new Client({ token, autorun: true });
const client = new Discord.Client();

// Log message once connected
client.on('ready', () => {
	log.info(`Connected as: ${client.user.username}#${client.user.discriminator}`);
});

// Watch for commands
client.on('message', message => {
	const user = message.author;
	const userKey = `${user.username}#${user.discriminator}`;
	const channel = message.channel;
	if (user.id === client.user.id) {
		return;
	}

	if (channel.type === "dm") {
		const content = message.content.trim();
		const args = content.split(" ");
		const command = args.shift();
		switch (command) {
			case "!key":
				if (data.get("assignments").hasOwnProperty(userKey)) {
					user.sendMessage(messages.keySent);
					break;
				}

				const availableKeys = keys(omitBy(data.get("keys"), (value, key) => !value));
				if (availableKeys.length === 0) {
					user.sendMessage(messages.outOfKeys);
					break;
				}

				const index = Math.floor(Math.random() * availableKeys.length);
				const assignedKey = availableKeys[index];
				data.set(`keys.${assignedKey}`, false);
				data.set(`assignments.${userKey}`, assignedKey);
				data.write(() => {
					user.sendMessage(template(messages.key, { assignedKey }));
				});

				break;
			case "!assign":
				if (args.length < 2) {
					log.warn(`Expected 2 arguments for !assign command from ${userKey}: ${content}`);
					break;
				}

				if (admins.indexOf(userKey) < 0) {
					log.warn(`Non-admin attempted to use !assign command: ${userKey}`);
					break;
				}

				data.set(`keys.${args[1]}`, false);
				data.set(`assignments.${args[0]}`, args[1]);
				data.write(() => {
					user.sendMessage("Assigned key to user");
				});

				break;
			case "!add-keys":
				if (admins.indexOf(userKey) < 0) {
					log.warn(`Non-admin attempted to use !add-keys command: ${userKey}`);
					break;
				}

				args.forEach(key => data.set(`keys.${key}`, true));
				data.write(() => {
					user.sendMessage("Added keys to the system");
				});

				break;
			default:
				log.info(`Unrecognized command from ${userKey}: ${command}`);
		}
	} else if (channel.type === "text" && channel.name === betaChannel) {
		const dmSent = data.get(`dmSent.${userKey}`);
		if (!dmSent) {
			data.set(`dmSent.${userKey}`, true);
			data.write(() => {
				user.sendMessage(messages.welcome);
			});
		}
	}
});

client.login(token);
